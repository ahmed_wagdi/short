import {ADD_LINK, CLEAR_LINKS, GET_STATS} from './types';
import axios from 'axios';

export function addLink(url){
    return (dispatch) => {
        axios.post(`/shorten`, { url }).then(response => {
            if(response.status === 201){
                dispatch({type: ADD_LINK, payload: {
                    original: url,
                    shortcode: response.data.result.shortcode,
                    redirectCount: 0,
                    startDate: new Date().toISOString()
                }
                })
            }

        });
    }
}

export function linkStats(shortcode, index){
    return (dispatch) => {
        axios.get(`/${shortcode}/stats`).then(response => {
            if(response.status !== 404){
                dispatch({type: GET_STATS, index, payload: response.data.result
                })
            }
        });
    }
}

export function clearLinks(){
   return { type: CLEAR_LINKS };
}