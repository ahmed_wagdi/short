import React from 'react';
import LinkItem from './LinkItem';
import {connect} from 'react-redux';
import {clearLinks} from "../actions";

function LinksTable(props){
    if(!props.links.length) return null;

    return(
        <div>
            <h4>Previously shortened by you <a onClick={() => props.clearLinks()}>Clear History</a></h4>
            <table className="links_table">
                <thead>
                    <tr>
                        <th>Link</th>
                        <th>Visits</th>
                        <th>Last Visited</th>
                    </tr>
                </thead>

                <tbody>
                    {props.links.map((link, i) => <LinkItem index={i} key={i} link={link} />)}
                </tbody>
            </table>
        </div>
    )
}
function mapStateToProps(state){
    return{
        links: state.links
    }
}
export default connect(mapStateToProps, {clearLinks})(LinksTable);