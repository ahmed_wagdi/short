import React from 'react';
import {connect} from 'react-redux';
import {addLink} from "../actions";

class Form extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            url: ""
        }
    }

    submitForm(e){
        e.preventDefault();
        const link = this.state.url;
        if(!link){
            alert("Please enter a url");
            return;
        }

        this.props.addLink(link)
        this.setState({url: ""});
    }

    render(){
        return(
            <form className="links_form" onSubmit={(e) => this.submitForm(e)}>
                <input value={this.state.url} onChange={(e) => this.setState({url: e.target.value})} className="links_form__text" type="text" placeholder="Enter link..." />
                <input className="links_form__submit button" disabled={!this.state.url} type="submit" value="Shorten this link" />
            </form>
        )
    }
}

export default connect(null, {addLink})(Form);