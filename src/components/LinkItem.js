import React from 'react';
import {linkStats} from '../actions';
import {connect} from 'react-redux';
import moment from 'moment';

class LinkItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            copy: false
        }
    }

    componentDidMount(){
        this.props.linkStats(this.props.link.shortcode, this.props.index);
        this.url = `https://impraise-shorty.herokuapp.com/${this.props.link.shortcode}`
    }

    copy(e){
        const textField = document.createElement("textarea")
        textField.innerText = this.url;
        document.body.appendChild(textField)
        textField.select();
        document.execCommand('copy');
        document.body.removeChild(textField);
        this.setState({copy: true});
        setTimeout(() => this.setState({copy: false}), 1000);
    }

    render(){
        const {link} = this.props;
        let {original} = link;
        
        // Limit original url to 50 characters
        const limit = 50;
        if(original.length > limit){
            original = original.substr(0, limit) + "..."
        }

        let date;
        let new_url = false;

        // Format Date
        if(this.props.link.startDate) {
            const startDate = moment(this.props.link.startDate)
            date = startDate.fromNow();
            const now = moment()
            new_url = now.diff(startDate, "minutes") < 10 ? true : false;
        }
        
        return(
            <tr className="link_item">
                <td className={`link_item__main ${this.state.copy ? "link_item__main--copy" : ""} ${new_url ? "link_item__main--new" : ""}`}>
                    <a className="link_item__link" onClick={(e) => this.copy(e)}>
                    https://impraise-shorty.herokuapp.com/<span>{this.props.link.shortcode}</span>
                        <span className="link_item__copy">Click to copy this link</span>
                    </a>
                    <div className="link_item__original">
                        {original}
                    </div>
                </td>
                <td className="link_item__visits">{link.redirectCount}</td>
                <td className="link_item__time">{date}</td>
            </tr>
        )
    }
}

export default connect(null, {linkStats})(LinkItem);