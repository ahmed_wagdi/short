import React, { Component } from 'react';
import Form from './Form';
import LinksTable from './LinksTable';

class App extends Component {
  render() {
    return (
      <div className="app">
        <div className="header">
          <h1>Shooooort</h1>
          <span>The link shortener with a long name</span>
        </div>
        <Form />
        <LinksTable />
      </div>
    );
  }
}

export default App;
