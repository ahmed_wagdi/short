import React from 'react';
import ReactDOM from 'react-dom';
import './css/reset.css';
import './css/index.css';
import App from './components/App';
import {createStore, applyMiddleware, compose} from 'redux';
import {persistStore, autoRehydrate} from 'redux-persist'
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

const store = createStore(reducers, undefined,
    compose(
      applyMiddleware(thunk),
      autoRehydrate()
    )
);

persistStore(store);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
