import {ADD_LINK, CLEAR_LINKS, GET_STATS} from '../actions';

export default function linksReducer(state = [], action){
    switch(action.type){
        case ADD_LINK:
            return [action.payload, ...state];

        case CLEAR_LINKS:
            return [];

        case GET_STATS:
            const {index} = action;
            const links = [...state];
            links[index] = {
                ...links[index],
                ...action.payload
            }

            
            return links;

        default:
            return state;
    }
    
}