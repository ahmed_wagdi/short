var express = require('express')
var requestLib = require('request');
var bodyParser = require('body-parser')
var app = express()

app.use( bodyParser.json() ); 

app.use(bodyParser.urlencoded({     
  extended: true
})); 

app.set('port', (process.env.PORT || 8080))



app.post('/shorten', function(request, response) {
    console.log(request.body);
    const {url, shortcode} = request.body;
    if(!url) response.status(400).json({result: "url is not present"});

    var options = {
        url: 'https://impraise-shorty.herokuapp.com/shorten',
        method: 'POST',
        json: true,
        body: {
            "url": url,
            "shortcode" : shortcode
        }
    }


    // Start the request
    requestLib(options, function (error, httpResponse, body) {
        response.status(httpResponse.statusCode).json({result: httpResponse.body});
    })
  
})

app.get('/:shortcode/stats', function(request, response) {
    const {shortcode} = request.params;
    
    var options = {
        url: `https://impraise-shorty.herokuapp.com/${shortcode}/stats`,
        method: 'GET'
    }

    requestLib(options, function (error, httpResponse, body) {
        response.status(httpResponse.statusCode).json({result: JSON.parse(httpResponse.body)});
    })
  
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})

