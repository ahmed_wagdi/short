## Run instructions
1. Clone Repo
2. Go into the project directory
3. Run `npm install`
4. Run `npm start`
5. `cd server`
6. Run `npm install`
7. Run `npm start`

## Libraries/Tools used:
**Backend:**

1. Node
2. Express

**Frontend:**

1. React
2. Redux
3. Redux Thunk
4. Redux Persist
5. Momentjs
6. Create react app
7. Axios